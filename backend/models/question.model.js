var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')

//Question schema
var QuestionSchema = new mongoose.Schema({
    title: String,
    description: String,
    rate: Number,
    created: Date,
    isOpen: Boolean
});

QuestionSchema.plugin(mongoosePaginate)
const Question = mongoose.model('Question', QuestionSchema)

module.exports = Question;