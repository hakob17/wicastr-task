let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');

//Comment schema
let CommentSchema = new mongoose.Schema({
    questionId: String,
    userId: String,
    text: {type: String, required: true},
    created: Date
});

CommentSchema.plugin(mongoosePaginate)
const Comment = mongoose.model('Comment', CommentSchema)

module.exports = Comment;