let Roles = [
    'attendee',
    'speaker',
    'moderator'
];
module.exports = {
    getRoles: function () {
        return Roles;
    }
};