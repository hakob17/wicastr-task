let Question = require('../models/question.model');

// Saving the context of this module inside the _the variable
_this = this;

// Async function to get the To do List
exports.getQuestions = async function(query, page, limit){

    // Options setup for the mongoose paginate
    let options = {
        page,
        limit
    };

    // Try Catch the awaited promise to handle the error

    try {

        // Return the todod list that was retured by the mongoose promise
        return await Question.paginate(query, options);

    } catch (e) {

        // return a Error message describing the reason
        throw Error('Error while Paginating Todos')
    }
};

exports.createQuestion = async function(question){

    // Creating a new Mongoose Object by using the new keyword
    let newQuestion = new Question({
        title: question.title,
        description: question.description,
        isOpen: true,
        rate: question.rate,
        created: Date.now()
    });

    try{

        // Saving the Question
        return await newQuestion.save();
    }catch(e){

        // return a Error message describing the reason
        throw Error("Error while Creating Question")
    }
};

exports.updateQuestion= async function(question){
    let oldQuestion;
    let id = question.id;

    try{
        //Find the old Question Object by the Id

        oldQuestion = await Question.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the Question")
    }

    // If no old Question Object exists return false
    if(!oldQuestion){
        return false;
    }

    console.log(oldQuestion);

    //Edit the Question Object
    oldQuestion.title = question.title;
    oldQuestion.description = question.description;
    oldQuestion.isOpen = question.isOpen;
    oldQuestion.rate = question.rate;
    oldQuestion.created = Date.now();


    console.log(oldQuestion);

    try{
        return await oldQuestion.save();
    }catch(e){
        throw Error("And Error occured while updating the Question");
    }
};

exports.deleteQuestion = async function(id){

    // Delete the Question
    try{
        let deleted = await Question.remove({_id: id});
        if(deleted.result.n === 0){
            throw Error("Question Could not be deleted")
        }
        return deleted;
    }catch(e){
        throw Error("Error Occured while Deleting the Question")
    }
};