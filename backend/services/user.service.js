let User = require('../models/user.model');
let Roles = require('../models/roles.model');

// Saving the context of this module inside the _the variable
_this = this;

// Async function to get the Users List
exports.getUsers = async function(query, page, limit){

    // Options setup for the mongoose paginate
    let options = {
        page,
        limit
    };

    // Try Catch the awaited promise to handle the error

    try {

        // Return the todod list that was retured by the mongoose promise
        return await User.paginate(query, options);

    } catch (e) {

        // return a Error message describing the reason
        throw Error('Error while Paginating Todos')
    }
};

exports.createUser = async function(user){

    // Creating a new Mongoose Object by using the new keyword
    console.log(Roles.getRoles());
    let newUser= new User({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: user.password,
        role: Roles.getRoles().indexOf(user.role) !== -1 ? user.role : Roles.getRoles()[0]
    });

    try{

        // Saving the User
        return await newUser.save();
    }catch(e){

        // return a Error message describing the reason
        throw Error("Error while Creating User")
    }
};

exports.updateUser= async function(user){
    let oldUser;
    let id = user.id;

    try{
        //Find the old User Object by the Id

        oldUser = await User.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the User")
    }

    // If no old User Object exists return false
    if(!oldUser){
        return false;
    }

    console.log(oldUser);

    //Edit the User Object
    oldUser.firstName = user.firstName;
    oldUser.lastName = user.lastName;
    oldUser.email = user.email;
    oldUser.password= user.password,
    oldUser.role = Roles.getRoles().indexOf(user.role) !== -1 ? user.role : oldUser.role;


    console.log(oldUser);

    try{
        return await oldUser.save();
    }catch(e){
        throw Error("And Error occured while updating the User");
    }
};

exports.deleteUser= async function(id){

    // Delete the User
    try{
        let deleted = await User.remove({_id: id});
        if(deleted.result.n === 0){
            throw Error("User Could not be deleted")
        }
        return deleted;
    }catch(e){
        throw Error("Error Occured while Deleting the User")
    }
};