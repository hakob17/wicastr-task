let Comment = require('../models/comment.model');
let Roles = require('../models/roles.model');

// Saving the context of this module inside the _the variable
_this = this;

// Async function to get the Comments List
exports.getComments = async function(query, page, limit){

    // Options setup for the mongoose paginate
    let options = {
        page,
        limit
    };

    // Try Catch the awaited promise to handle the error

    try {

        // Return the Comment list that was returned by the mongoose promise
        return await Comment.paginate(query, options);

    } catch (e) {

        // return a Error message describing the reason
        throw Error('Error while Paginating Todos')
    }
};

exports.createComment = async function(comment){

    // Creating a new Mongoose Object by using the new keyword
    let newComment = new Comment({
        questionId: comment.questionId,
        userId: comment.lastName,
        text: comment.text,
        created: Date.now()
    });

    try{

        // Saving the Comment
        return await newComment.save();
    }catch(e){

        // return a Error message describing the reason
        throw Error("Error while Creating Comment")
    }
};

exports.updateComment= async function(comment){
    let oldComment;
    let id = comment.id;

    try{
        //Find the old User Object by the Id

        oldComment = await Comment.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the Comment")
    }

    // If no old User Object exists return false
    if(!oldComment){
        return false;
    }

    //Edit the Comment Object
    oldComment.text = comment.text;

    try{
        return await oldComment.save();
    }catch(e){
        throw Error("And Error occured while updating the Comment");
    }
};

exports.deleteComment= async function(id){

    // Delete the Comment
    try{
        let deleted = await Comment.remove({_id: id});
        if(deleted.result.n === 0){
            throw Error("Comment Could not be deleted")
        }
        return deleted;
    }catch(e){
        throw Error("Error Occured while Deleting the Comment")
    }
};