let express = require('express');

let router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var database = require('../config/database');
var jwt = require('jsonwebtoken');
let questions = require('./api/questions.api.route');
let users = require('./api/users.api.route');
let User = require('../models/user.model');
let UserController = require('../controllers/user.controller');
let node_acl = require('acl');
var acl;
//Connect to database
mongoose.connect(database.database, _mongo_connected)
    .then(() => {
        console.log(`Succesfully Connected to the Mongodb Database  at URL : ` + database.database)
    })
    .catch(() => {
        console.log(`Error Connecting to the Mongodb Database at URL : ` + database.database)
    });

function _mongo_connected(error, db) {

    var mongoBackend = new node_acl.mongodbBackend(db /*, {String} prefix */);

    // Create a new access control list by providing the mongo backend
    //  Also inject a simple logger to provide meaningful output
    acl = new node_acl(mongoBackend);

    // Defining roles and routes
    set_roles();

    router.use('/questions', [
        passport.authenticate('jwt', {session: false})
    ], questions);
    router.use('/users', passport.authenticate('jwt', {session: false}), users);
    router.use('/account', passport.authenticate('jwt', {session: false}), function (req,res,next) {
        // Id is necessary for the update
        if (req.headers && req.headers.authorization) {
            var authorization = req.headers.authorization,
                decoded;
            try {
                decoded = jwt.verify(authorization.replace("Bearer ",""), database.secret);
            } catch (e) {
                return res.status(401).send('unauthorized');
            }

           return res.json({status: 200, message: "account", account: decoded});
        }
        return res.status(401).send('unauthorized');
    });
}
function set_roles() {

    // Define roles, resources and permissions
    acl.allow([
        {
            roles: 'moderator',
            allows: [
                {resources: '/api', permissions: '*'}
            ]
        }, {
            roles: 'speaker',
            allows: [
                {resources: '/api/questions', permissions: ['get', 'create', 'update']}
            ]
        }, {
            roles: 'attendee',
            allows: [
                {resources: '/api/questions', permissions: ['get','create']}
            ]
        }
    ]);

    // Inherit roles
    //  Every user is allowed to do what guests do
    // //  Every admin is allowed to do what users do
    // acl.addRoleParents( 'user', 'guest' );
    // acl.addRoleParents( 'admin', 'user' );
}

//Login route
router.post('/login', function (req, res) {
    console.log(req.body);
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
            // check if password matches
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    let token = jwt.sign(user.toJSON(), config.secret);
                    // return the information including token as JSON
                    res.json({success: true, token: 'Bearer ' + token});
                } else {
                    res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
                }
            });
        }
    });
});

//Register Route
router.post('/register', UserController.createUser);

module.exports = router;