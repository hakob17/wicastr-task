let express = require('express');

let router = express.Router();

// Getting the Users Controller
let UsersController = require('../../controllers/user.controller');


// Map each API to the Controller Functions
router.get('/',is_allowed, UsersController.getUsers);
router.post('/',UsersController.createUser);
router.put('/',is_allowed, UsersController.updateUser);
router.delete('/:id',is_allowed, UsersController.removeUser);


function is_allowed(req, res,next) {
    if (req.headers && req.headers.authorization) {
        var authorization = req.headers.authorization,
            decoded;
        try {
            decoded = jwt.verify(authorization.replace("Bearer ",""), database.secret);
        } catch (e) {
            return res.status(401).send('unauthorized');
        }

        if (decoded.role == "moderator") {
            return next(req, res);
        }else {
            return res.status(401).send("Don't have permissions");
        }
    }
    return res.status(401).send('unauthorized');
}
// Export the Router
module.exports = router;