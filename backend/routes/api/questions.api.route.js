let express = require('express');

var jwt = require('jsonwebtoken');
var database = require('../../config/database');
let router = express.Router();

// Getting the Question Controller
let QuestionController = require('../../controllers/question.controller');


// Map each API to the Controller Functions
router.get('/', QuestionController.getQuestions);
router.post('/', QuestionController.createQuestion);
router.put('/',is_allowed, QuestionController.updateQuestion);
router.delete('/:id',is_allowed, QuestionController.removeQuestion);

function is_allowed(req, res,next) {
    if (req.headers && req.headers.authorization) {
        var authorization = req.headers.authorization,
            decoded;
        try {
            decoded = jwt.verify(authorization.replace("Bearer ",""), database.secret);
        } catch (e) {
            return res.status(401).send('unauthorized');
        }

        if (decoded.role == "moderator" || decoded.role == "speaker") {
            return next(req, res);
        }else {
            return res.status(401).send("Don't have permissions");
        }
    }
    return res.status(500).send('Something went wrong');
}
// Export the Router
module.exports = router;