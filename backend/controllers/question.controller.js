let QuestionService = require('../services/question.service');

// Saving the context of this module inside the _the variable
_this = this;


// Async Controller function to get the Question List
exports.getQuestions = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : 10;

    try{

        const questions = await QuestionService.getQuestions({}, page, limit);

        // Return the question list with the appropriate HTTP Status Code and Message.

        return res.status(200).json({status: 200, data: questions, message: "Succesfully Questions Recieved"});

    }catch(e){

        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: e.message});

    }
};

exports.createQuestion = async function(req, res, next){

    // Req.Body contains the form submit values.
    let question = {
        title: req.body.title,
        description: req.body.description,
        rate: req.body.rate
    };

    try{

        // Calling the Service function with the new object from the Request Body
        let createdQuestion = await QuestionService.createQuestion(question);
        return res.status(201).json({status: 201, data: createdQuestion, message: "Succesfully Created Question"})
    }catch(e){

        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: "Question Creation was Unsuccesfull"})
    }
};

exports.updateQuestion = async function(req, res, next){

    // Id is necessary for the update
    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    let id = req.body._id;

    console.log(req.body);

    let question = {
        id,
        title: req.body.title ? req.body.title : null,
        description: req.body.description ? req.body.description : null,
        isOpen: req.body.isOpen ? req.body.isOpen : null,
        rate: req.body.rate ? req.body.rate : null
    };

    try{
        let updatedQuestion = await QuestionService.updateQuestion(question);
        return res.status(200).json({status: 200, data: updatedQuestion, message: "Succesfully Updated Question"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
};

exports.removeQuestion = async function(req, res, next){

    let id = req.params.id;

    try{
        let deleted = await QuestionService.deleteQuestion(id);
        return res.status(204).json({status:204, message: "Succesfully Question  Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

};