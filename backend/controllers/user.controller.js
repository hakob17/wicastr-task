let UserService = require('../services/user.service');

// Saving the context of this module inside the _the variable
_this = this;


// Async Controller function to get the Question List
exports.getUsers = async function (req, res, next) {

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : 10;

    try {

        const questions = await UserService.getUsers({}, page, limit);

        // Return the question list with the appropriate HTTP Status Code and Message.

        return res.status(200).json({status: 200, data: questions, message: "Users Succesfully  Recieved"});

    } catch (e) {

        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: e.message});

    }
};

exports.createUser = async function (req, res, next) {
    // Req.Body contains the form submit values.
    let user = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role
    };

    try {

        // Calling the Service function with the new object from the Request Body
        let createdUser = await UserService.createUser(user);
        console.log(createdUser);
        return res.status(201).json({status: 201, data: createdUser, message: "Succesfully Created User"})
    } catch (e) {

        console.log(e);
        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: "User Creation was Unsuccesfull"})
    }
};

exports.updateUser = async function (req, res, next) {

    // Id is necessary for the update
    if (!req.body._id) {
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    let id = req.body._id;

    console.log(req.body);

    let user = {
        id,
        firstName: req.body.firstName ? req.body.firstName : null,
        lastName: req.body.lastName ? req.body.lastName : null,
        email: req.body.email ? req.body.email : null,
        password: req.body.password ? req.body.password : null,
        role: req.body.role ? req.body.role : null
    };

    try {
        let updatedUser = await updatedUser.update(user);
        return res.status(200).json({status: 200, data: updatedUser, message: "Succesfully Updated User"})
    } catch (e) {
        return res.status(400).json({status: 400., message: e.message})
    }
};



exports.removeUser = async function (req, res, next) {

    let id = req.params.id;

    try {
        let deleted = await UserService.deleteUser(id);
        return res.status(204).json({status: 204, message: "User Succesfully Deleted"})
    } catch (e) {
        return res.status(400).json({status: 400, message: e.message})
    }

};