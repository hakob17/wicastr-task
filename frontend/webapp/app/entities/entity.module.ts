import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { WicastrtaskUserModule } from './user/user.module';
import { WicastrtaskQuestionModule } from './question/question.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        WicastrtaskUserModule,
        WicastrtaskQuestionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WicastrtaskEntityModule {}
