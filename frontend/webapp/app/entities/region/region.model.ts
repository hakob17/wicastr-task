import { BaseEntity } from './../../shared';

export class Region implements BaseEntity {
    constructor(
        public id?: string,
        public regionName?: string,
    ) {
    }
}
