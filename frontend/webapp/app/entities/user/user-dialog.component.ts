import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { User } from './user.model';
import { UserPopupService } from './user-popup.service';
import { UserService } from './user.service';

@Component({
    selector: 'jhi-user-dialog',
    templateUrl: './user-dialog.component.html'
})
export class UserDialogComponent implements OnInit {

    user: User;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.user.id !== undefined) {
            this.subscribeToSaveResponse(
                this.userService.update(this.user));
        } else {
            this.subscribeToSaveResponse(
                this.userService.create(this.user));
        }
    }

    private subscribeToSaveResponse(result: Observable<User>) {
        result.subscribe((res: User) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: User) {
        this.eventManager.broadcast({ name: 'userListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-user-popup',
    template: ''
})
export class UserPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userPopupService: UserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.userPopupService
                    .open(UserDialogComponent as Component, params['id']);
            } else {
                this.userPopupService
                    .open(UserDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
