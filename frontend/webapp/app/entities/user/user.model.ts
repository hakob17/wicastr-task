import { BaseEntity } from './../../shared';

export const enum Role {
    'ATTENDEE',
    ' SPEAKER',
    ' MODERATOR'
}

export class User implements BaseEntity {
    constructor(
        public id?: string,
        public role?: Role,
        public firstName?: string,
        public lastName?: string,
        public isActive?: boolean,
        public email?: string,
        public password?: string,
        public login?: string,
    ) {
        this.isActive = false;
    }
}
