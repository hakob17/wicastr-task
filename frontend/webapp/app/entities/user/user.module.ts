import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WicastrtaskSharedModule } from '../../shared';
import {
    UserService,
    UserPopupService,
    UserComponent,
    UserDetailComponent,
    UserDialogComponent,
    UserPopupComponent,
    UserDeletePopupComponent,
    UserDeleteDialogComponent,
    userRoute,
    userPopupRoute,
} from './';

const ENTITY_STATES = [
    ...userRoute,
    ...userPopupRoute,
];

@NgModule({
    imports: [
        WicastrtaskSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UserComponent,
        UserDetailComponent,
        UserDialogComponent,
        UserDeleteDialogComponent,
        UserPopupComponent,
        UserDeletePopupComponent,
    ],
    entryComponents: [
        UserComponent,
        UserDialogComponent,
        UserPopupComponent,
        UserDeleteDialogComponent,
        UserDeletePopupComponent,
    ],
    providers: [
        UserService,
        UserPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WicastrtaskUserModule {}
