import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { QuestionMySuffix } from './question-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class QuestionMySuffixService {

    private resourceUrl =  SERVER_API_URL + 'api/questions';

    constructor(private http: Http) { }

    create(question: QuestionMySuffix): Observable<QuestionMySuffix> {
        const copy = this.convert(question);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(question: QuestionMySuffix): Observable<QuestionMySuffix> {
        const copy = this.convert(question);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<QuestionMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to QuestionMySuffix.
     */
    private convertItemFromServer(json: any): QuestionMySuffix {
        const entity: QuestionMySuffix = Object.assign(new QuestionMySuffix(), json);
        return entity;
    }

    /**
     * Convert a QuestionMySuffix to a JSON which can be sent to the server.
     */
    private convert(question: QuestionMySuffix): QuestionMySuffix {
        const copy: QuestionMySuffix = Object.assign({}, question);
        return copy;
    }
}
