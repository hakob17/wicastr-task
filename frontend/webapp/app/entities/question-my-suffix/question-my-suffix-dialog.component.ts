import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { QuestionMySuffix } from './question-my-suffix.model';
import { QuestionMySuffixPopupService } from './question-my-suffix-popup.service';
import { QuestionMySuffixService } from './question-my-suffix.service';

@Component({
    selector: 'jhi-question-my-suffix-dialog',
    templateUrl: './question-my-suffix-dialog.component.html'
})
export class QuestionMySuffixDialogComponent implements OnInit {

    question: QuestionMySuffix;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private questionService: QuestionMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.question.id !== undefined) {
            this.subscribeToSaveResponse(
                this.questionService.update(this.question));
        } else {
            this.subscribeToSaveResponse(
                this.questionService.create(this.question));
        }
    }

    private subscribeToSaveResponse(result: Observable<QuestionMySuffix>) {
        result.subscribe((res: QuestionMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: QuestionMySuffix) {
        this.eventManager.broadcast({ name: 'questionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-question-my-suffix-popup',
    template: ''
})
export class QuestionMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private questionPopupService: QuestionMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.questionPopupService
                    .open(QuestionMySuffixDialogComponent as Component, params['id']);
            } else {
                this.questionPopupService
                    .open(QuestionMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
