import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { QuestionMySuffixComponent } from './question-my-suffix.component';
import { QuestionMySuffixDetailComponent } from './question-my-suffix-detail.component';
import { QuestionMySuffixPopupComponent } from './question-my-suffix-dialog.component';
import { QuestionMySuffixDeletePopupComponent } from './question-my-suffix-delete-dialog.component';

export const questionRoute: Routes = [
    {
        path: 'question-my-suffix',
        component: QuestionMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Questions'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'question-my-suffix/:id',
        component: QuestionMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Questions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const questionPopupRoute: Routes = [
    {
        path: 'question-my-suffix-new',
        component: QuestionMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Questions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'question-my-suffix/:id/edit',
        component: QuestionMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Questions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'question-my-suffix/:id/delete',
        component: QuestionMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Questions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
