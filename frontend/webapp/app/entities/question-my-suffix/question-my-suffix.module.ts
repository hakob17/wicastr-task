import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WicastrtaskSharedModule } from '../../shared';
import {
    QuestionMySuffixService,
    QuestionMySuffixPopupService,
    QuestionMySuffixComponent,
    QuestionMySuffixDetailComponent,
    QuestionMySuffixDialogComponent,
    QuestionMySuffixPopupComponent,
    QuestionMySuffixDeletePopupComponent,
    QuestionMySuffixDeleteDialogComponent,
    questionRoute,
    questionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...questionRoute,
    ...questionPopupRoute,
];

@NgModule({
    imports: [
        WicastrtaskSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        QuestionMySuffixComponent,
        QuestionMySuffixDetailComponent,
        QuestionMySuffixDialogComponent,
        QuestionMySuffixDeleteDialogComponent,
        QuestionMySuffixPopupComponent,
        QuestionMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        QuestionMySuffixComponent,
        QuestionMySuffixDialogComponent,
        QuestionMySuffixPopupComponent,
        QuestionMySuffixDeleteDialogComponent,
        QuestionMySuffixDeletePopupComponent,
    ],
    providers: [
        QuestionMySuffixService,
        QuestionMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WicastrtaskQuestionMySuffixModule {}
