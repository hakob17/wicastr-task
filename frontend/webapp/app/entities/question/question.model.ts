import { BaseEntity } from './../../shared';

export class Question implements BaseEntity {
    constructor(
        public id?: string,
        public title?: string,
        public description?: string,
        public isOpen?: boolean,
        public rate?: number,
    ) {
        this.isOpen = false;
    }
}
