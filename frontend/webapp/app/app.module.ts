import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ngx-webstorage';

import { WicastrtaskSharedModule, UserRouteAccessService } from './shared';
import { WicastrtaskAppRoutingModule} from './app-routing.module';
import { WicastrtaskHomeModule } from './home/home.module';
import { WicastrtaskAdminModule } from './admin/admin.module';
import { WicastrtaskAccountModule } from './account/account.module';
import { WicastrtaskEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        WicastrtaskAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        WicastrtaskSharedModule,
        WicastrtaskHomeModule,
        WicastrtaskAdminModule,
        WicastrtaskAccountModule,
        WicastrtaskEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class WicastrtaskAppModule {}
